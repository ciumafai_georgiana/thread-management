package Model;

import Model.Client;
import View.View;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static java.lang.Thread.sleep;

public class Queue extends Thread {

    private int queueId;
    private BlockingQueue<Client> clients=new ArrayBlockingQueue<Client>(100);
    private int nbClients=0;
    private int waitingPeriod;
    private int totalWaiting=0;
    private int totalService=0;
    private int curentTime = 0;
    private int simulationInterval ;
    public View v;
    public long startTime;
    public long endTime;
    //imi bag waiting time si alea aici le calculez in interiorul queueul
    //am o metoda de get pt fiecare pe care o apelez din sim manager
    //

    public Queue(int queueId,int simulationInterval,View v) {
        this.queueId = queueId;
        System.out.println("Queue: " + this.queueId + " created");
        System.out.println();
        this.simulationInterval=simulationInterval;
        v=v;

    }

    public void run() {
        System.out.println("Queue: " + this.queueId + " started");
        try {
            while (curentTime<simulationInterval) {

                client_process();
                int svTime=clients.take().getServiceTime();
                //v.appendToTextField("waiting period for client " + clients.get(0).getClientId()+ "is " + waitingPeriod);
                //System.out.println("service time for client: "+clients.get(0).getClientId()+" is "+clients.get(0).getServiceTime());
               // sterge_client();
               // System.out.println("bf sleep"+queueId);
                sleep(svTime*1000000);

                curentTime++;
                if(clients.size()!=0)
                  waitingPeriod=curentTime+clients.take().getServiceTime()+clients.take().getArrivingTime();
                    //deci waiting time/period il calculez bine pentru fiecare client

            }
        } catch(InterruptedException e){
                System.out.println("Intrerupere");
                System.out.println(e.toString());
            }
        if (curentTime<simulationInterval){
            //System.out.println("ifthread");
            this.interrupt();
        }
    }
    //adaug un client nou
    public synchronized void client_process() throws InterruptedException
    {
        // Daca nu avem clienti la coada punem threadul sa astepte
        while(clients.isEmpty()) {
            wait();
            System.out.println("orice");
        }
        //Preluam informatiile din primul element din coada
        Client client=clients.take();
        totalService+=client.getServiceTime();


        notifyAll();

    }

    public int getWaitingPeriod() {
        return this.waitingPeriod;
    }

    public int getTotalWaiting() {
        return this.totalWaiting;
    }

    public int getTotalService() {
        return totalService;
    }

    public int getCurentTime() {
        return curentTime;
    }

    public synchronized void adauga_client(Client c) throws InterruptedException
    {

        clients.put(c);
        nbClients++;
       //System.out.println("Model.Client: " + c.getClientId() + " arrived at queue: " + this.queueId);
       notifyAll();
    }
    //sterg un client
    public synchronized void sterge_client() throws InterruptedException
    {
        //System.out.println( clients.get(0).getClientId()+" a fost deservit de casa "+queueId);
        clients.take();

        //notifyAll();
    }
    public synchronized int noClients() {

        int size = clients.size();
        return size;
    }
    public double getAverageServiceTime(int noClients) {
        if(noClients()!=0)
            return (double)totalService/noClients();
        return 0;
    }

    public int getClientNumber() {
        return this.nbClients;
    }
}