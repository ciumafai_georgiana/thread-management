package Model;

import Model.Client;
import Model.Queue;
import View.View;

import java.util.Random;

public class SimulationManager extends Thread {
    private int nrOfQueues;
    public Queue[] queues ;
    public int curentTime=0;
    public int simulationInterval;
    public int MaximArrival;
    public int MinimArrival;
    public int MaximService;
    public int MinimService;
    public View v;
    public int nrC;
    public Client c;
    public long startTime;
    public long endTime;
    public SimulationManager(int nrOfQueues,int nrC,int simulationInterval,int MaximArrival,int MinimArrival,int MaximService,int MinimService,View v){
        this.nrOfQueues = nrOfQueues;
        queues = new Queue[this.nrOfQueues];
        System.out.println("SimulationManager");
        this.simulationInterval=simulationInterval;
        this.MaximArrival=MaximArrival;
        this.MinimArrival=MinimArrival;
        this.MaximService=MaximService;
        this.MinimService=MinimService;
        this.nrC=nrC;
        this.v=v;
        startTime=System.nanoTime();
    }

    public void addClient(Client c){
        int min=queues[0].getClientNumber();
        System.out.println("min clients:" + min);
        for(int i=0;i<nrOfQueues;i++){
            if(queues[i].getClientNumber() < min){
                min = i;
            }
        }
        try {
            queues[min].adauga_client(c);
            curentTime++;

            int wpc=c.getServiceTime()+c.getArrivingTime();
            int lvt=wpc+queues[min].getCurentTime();
            //cum plm calculez leaving time
            v.appendToTextField("Client "+c.getClientId()+" arrived at queue " + min + " having service time: "+c.getServiceTime()+"\n");
           // v.appendToTextField("Client "+c.getClientId()+" has left the queue "+ min+ "at time"+lvt+"\n");
            //v.appendToTextField("waiting period"+wpc);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
//un thread din procesor imi e over clock si de asta imi sunt mai multi clienti in un thread
    public void generateClients(int idClient){
        Random r=new Random();
        int serviceTime=r.nextInt(this.MaximService-this.MinimService+1)+this.MinimService;
        int arrivalTime=r.nextInt(this.MaximArrival-this.MinimArrival+1)+this.MinimArrival;
        c=new Client(idClient,arrivalTime,serviceTime);
        addClient(c);
        try {
            sleep(c.getServiceTime()*1000);
            endTime=System.nanoTime();
            v.appendToTextField("end time "+(endTime-startTime)/1000000000+"\n");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
    public void run() {
        for (int i = 0; i < nrOfQueues; i++) {
            Queue qu = new Queue(i, simulationInterval, v);
            queues[i] = qu;
        }
        for (int i = 0; i < nrOfQueues; i++) {
            queues[i].start();
        }

        v.simulationWindow(nrOfQueues);

       while(curentTime < simulationInterval) {


        for (int i = 0; i < nrC; i++) {

            generateClients(i);

        }
        curentTime++;
        }

        for (int i = 0; i < nrOfQueues; i++) {
            try {
                System.out.println("Queue: " + i + " closed");
                queues[i].join();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
