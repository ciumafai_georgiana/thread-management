package Model;

public class Client {

    private int clientId;
    private int arrivingTime;
    private int serviceTime;

    public Client(int clientId, int arrivingTime, int serviceTime) {
        this.clientId = clientId;
        this.arrivingTime = arrivingTime;
        this.serviceTime = serviceTime;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getArrivingTime() {
        return arrivingTime;
    }

    public void setArrivingTime(int arrivingTime) {
        this.arrivingTime = arrivingTime;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }

    @Override
    public String toString() {
        return "Model.Client{" +
                "clientId=" + clientId +
                ", arrivingTime=" + arrivingTime +
                ", serviceTime=" + serviceTime +
                '}';
    }
}
