package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

public class View extends JPanel {
    private JFrame f = new JFrame("");
    private JFrame f2 = new JFrame("");
    private JPanel panel4 = new JPanel();
    private JPanel panel5=new JPanel();

    private JPanel panel = new JPanel();
    private JPanel panel1 = new JPanel();
    private JPanel panel2 = new JPanel();
    private JLabel label1 = new JLabel("Max Arriving:");
    private JLabel label2 = new JLabel("Min Arriving:");
    private JLabel label3 = new JLabel("Max Service:");
    private JLabel label4 = new JLabel("Min Service:");
    private JLabel label5 = new JLabel("Nb of Clients:");
    private JLabel label6 = new JLabel("Nb of Queues:");
    private JLabel label7 = new JLabel("Time:");
    private JLabel label8 = new JLabel("                ");
    private JTextField NbC=new JTextField(5);
    private JTextField MaxA = new JTextField(5);
    private JTextField MinA = new JTextField(5);
    private JTextField MaxS = new JTextField(5);
    private JTextField MinS = new JTextField(5);
    private JTextField NbQ = new JTextField(5);
    private JTextField Time = new JTextField(5);
    private JButton Sim = new JButton("Start");
    //JTextArea loggerArea = new JTextArea();

    private JScrollPane scrollPane = new JScrollPane();
    private TextArea t=new TextArea();
    private TextArea t2=new TextArea();
    private JTextArea queDisp = new JTextArea(); //ce se intampla cu clientii pt fiecare coada
    private JTextArea simRes = new JTextArea(); //average waiting time, peak time


    public View() {
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         f.setSize(800, 800);
        f.add(panel);
        panel.add(panel1);
       // panel.add(panel2);
        panel1.setLayout(new BoxLayout(panel1,BoxLayout.Y_AXIS));
        label1.setAlignmentX(LEFT_ALIGNMENT);
        MaxA.setAlignmentX(LEFT_ALIGNMENT);
        label2.setAlignmentX(LEFT_ALIGNMENT);
        MinA.setAlignmentX(LEFT_ALIGNMENT);
        label3.setAlignmentX(LEFT_ALIGNMENT);
        MaxS.setAlignmentX(LEFT_ALIGNMENT);
        label4.setAlignmentX(LEFT_ALIGNMENT);
        MinS.setAlignmentX(LEFT_ALIGNMENT);
        label5.setAlignmentX(LEFT_ALIGNMENT);
        NbC.setAlignmentX(LEFT_ALIGNMENT);
        label6.setAlignmentX(LEFT_ALIGNMENT);
        NbQ.setAlignmentX(LEFT_ALIGNMENT);
        label7.setAlignmentX(LEFT_ALIGNMENT);
        Time.setAlignmentX(LEFT_ALIGNMENT);
        Sim.setAlignmentX(LEFT_ALIGNMENT);

        panel1.add(label1);

        panel1.add(MaxA);

         panel.add(label8);
        panel1.add(label2);
        panel1.add(MinA);
        panel1.add(label3);
        panel1.add(MaxS);
        panel1.add(label4);
        panel1.add(MinS);
        panel1.add(label5);
        panel1.add(NbC);
        panel1.add(label6);
        panel1.add(NbQ);
        panel1.add(label7);
       panel1.add(Time);
        panel1.add(Sim);

        //rezultatele afisate
       // scrollPane.setViewportView(queDisp);
       // panel2.add(scrollPane );

       // panel2.add(simRes);

        //f.add(panel2);

        f.pack();

        f.setVisible(true);
    }

    /*
    public JTextArea getQueDisp() {
        return queDisp;
    }


    public void setQueDisp(JTextArea queDisp) {
        this.queDisp = queDisp;
    }*/


    public int getMaxA() {
        return Integer.parseInt(MaxA.getText());
    }


    public void setMaxA(JTextField maxA) {
        this.MaxA = maxA;
    }


    public int getMinA() {
        return Integer.parseInt(MinA.getText());
    }

    public int getNbC() {
        return Integer.parseInt(NbC.getText());
    }

    public void setNbC(JTextField nbC) {
        NbC = nbC;
    }

    public void setMinA(JTextField minA) {
        this.MinA = minA;
    }


    public int getMaxS() {
        return Integer.parseInt(MaxS.getText());
    }


    public void setMaxS(JTextField maxS) {
        this.MaxS = maxS;
    }


    public int getMinS() {
        return Integer.parseInt(MinS.getText());
    }


    public void setMinS(JTextField minS) {
        this.MinS = minS;
    }

    public int getNbQ() {
        return Integer.parseInt(NbQ.getText());
    }


    public void setNbQ(JTextField nbQ) {
        this.NbQ = nbQ;
    }


    public int getTime() {
        return Integer.parseInt(Time.getText());
    }


    public void setTime(JTextField time) {
        this.Time = time;
    }
    public void addActionListener(ActionListener e) {
        Sim.addActionListener(e);
    }
    public void simulationWindow(int nrQ)
    {

       f2=new JFrame("");
        f2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //f.setSize(830, 400);
        panel4.add(t);
        panel4.setAlignmentX(RIGHT_ALIGNMENT);
        f2.add(panel4);
        f2.pack();

        f2.setVisible(true);

    }
    public void appendToTextField(String s){
        t.append(s);
    }
    /*public void appendToTextField2(String s){
        t2.append(s);
    }
    public void simulationWindow2(int nrQ)
    {

        f2=new JFrame("");
        f2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //f.setSize(830, 400);
        panel5.add(t);
        //panel5.setAlignmentY(BOTTOM_ALIGNMENT);
        f2.add(panel5);
        //f.pack();

        f2.setVisible(true);

    }*/
}

