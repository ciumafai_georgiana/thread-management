package Controller;

import Model.SimulationManager;
import View.View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {
    private View view;
    private SimulationManager sim;
    private int minA,maxA,minS,maxS,nrQ,nrC,time;
    public Controller(View view) {
        this.view = view;
        this.view.addActionListener(new ButtonLis());
    }

    class ButtonLis implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            minA=view.getMinA();
            maxA=view.getMaxA();
            minS=view.getMinS();
            maxS=view.getMaxS();
            time=view.getTime();
            nrQ=view.getNbQ();
            nrC=view.getNbC();
            sim = new SimulationManager(nrQ,nrC,time,maxA,minA,maxS,minS,view);
            if(sim!=null) {
                Thread thread = new Thread(sim);
                thread.start();

                //updateView();
            }
        }

    }


}



